package com.example.demo.GlobalException;

import java.util.NoSuchElementException;

import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.example.demo.customizedException.DataNotFoundException;

@RestControllerAdvice
public class GlobalException {
	
	@ExceptionHandler({InvalidDataAccessApiUsageException.class,DataNotFoundException.class,HttpRequestMethodNotSupportedException.class,NoSuchElementException.class})
	public ResponseEntity<String> invalidDataAccessApiUsageException(Exception ex){	
		return new ResponseEntity<>(ex.getMessage(),HttpStatus.BAD_REQUEST);
	}
	
}
