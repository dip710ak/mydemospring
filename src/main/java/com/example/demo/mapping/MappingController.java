package com.example.demo.mapping;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.customizedException.DataNotFoundException;
import com.example.demo.entity.Address;
import com.example.demo.entity.Person;
import com.example.demo.repo.AddressRepo;
import com.example.demo.repo.PersonRepo;

import jakarta.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/entity")
public class MappingController {

	@Autowired
	PersonRepo personRepo;
	
	@Autowired
	AddressRepo addressrepo;
	
	@Autowired
	NamedParameterJdbcTemplate tempate;

	@PostMapping("/person")
	public ResponseEntity<Person> savePersonEntity(@RequestBody Person person) {
		Person save = personRepo.save(person);
		return new ResponseEntity<Person>(save, HttpStatus.CREATED);
	}

	@GetMapping("/person")
	public ResponseEntity<Iterable<Person>> getallData() {
		Iterable<Person> findAll = personRepo.findAll();
		return new ResponseEntity<Iterable<Person>>(findAll, HttpStatus.OK);
	}

	@GetMapping("/person/{id}")
	public ResponseEntity<Optional<Person>> getPersonById(@PathVariable("id") Long id) throws Exception {
		Optional<Person> findById = personRepo.findById(id);
		if (findById.isEmpty()) {
			throw new DataNotFoundException("Data not Found");
		}
		return new ResponseEntity<Optional<Person>>(findById, HttpStatus.OK);
	}
	
	
	@PutMapping("/person/{id}")
	public ResponseEntity<Person> updatePersonEntity(@PathVariable("id") long id,@RequestBody Person person){
		System.out.println("___> "+person.toString());
		System.out.println("id____> "+id);
		Optional<Person> findById = personRepo.findById(id);
		if(findById.isEmpty()) {
			new DataNotFoundException("No Data...!!");
		}
		Person person2 = findById.get();
		person2.setId(id);
		person2.setFname(person.getFname());
		person2.setLname(person.getLname());
		Address address = person2.getAddress();
		address.setCity(person.getAddress().getCity());
		address.setHouseNo(person.getAddress().getHouseNo());
		address.setState(person.getAddress().getState());
		person2.setAddress(address);
		personRepo.save(person2);
		return new ResponseEntity<Person>(person2,HttpStatus.OK);
	}
	
	@DeleteMapping("/person/{id}")
	public ResponseEntity<String> deletePerson(@PathVariable("id") long id) throws DataNotFoundException{
		Optional<Person> findById = personRepo.findById(id);
		if(findById.isEmpty()) {
			throw new DataNotFoundException(String.format("Data not found with id = %s", id));
		}
		Person person = findById.get();
		personRepo.delete(person);
		return new ResponseEntity<String>("Successfully Deleted...!!",HttpStatus.OK);
	}
	
	
	@GetMapping("/address/all")
	public ResponseEntity<List<Address>> getAllAddress(){
		
		List<Address> findAll = addressrepo.findAll();
//		System.out.println("Address__> "+findAll.toString());
		return new ResponseEntity<List<Address>>(findAll,HttpStatus.OK);
	}
	
	@DeleteMapping("/address/{id}")
	public ResponseEntity<String> deleteAddress(@PathVariable("id") long id) throws Exception{
		Optional<Address> findById = addressrepo.findById(id);
		if(findById.isEmpty()) {
			throw new DataNotFoundException(String.format("Address with id = %s didn't found", id));  
		}
		Address address = findById.get();
		System.out.println("address__> "+address.toString());
		addressrepo.delete(address);
		return new ResponseEntity<String>("Deleted Successfull...",HttpStatus.OK);
	}
	
	@RequestMapping(path = "/address/{id}",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> getAllAddress(@PathVariable("id") long id,HttpServletResponse response) throws Exception{
		
		Map<String,Object> map = new HashMap<>();
		map.put("id", id);
		String query = "select addr.id adrId,addr.city city,addr.house_no houseno,addr.state state"
				+ ", per.id perId,per.fname,per.lname"
				+ " from address_info addr , person_info per where addr.id=per.address_id and addr.id=:id";
		System.out.println("Query___> "+query);
		List<JSONObject> list =new LinkedList<>();
		
		list=tempate.query(query, map, new ResultSetExtractor<List<JSONObject>>() {
			
			@Override
			public List<JSONObject> extractData(ResultSet rs) throws SQLException, DataAccessException {
				
				List<JSONObject> list = new LinkedList<>();
				while(rs.next()) {
					JSONObject obj = new JSONObject();
					obj.put("address_id", rs.getLong("adrId"));
					obj.put("city",rs.getString("city"));
					obj.put("House_No",rs.getString("houseno"));
					obj.put("State",rs.getString("state"));
					obj.put("FirstName",rs.getString("fname"));
					obj.put("LastName",rs.getString("lname"));
					obj.put("PersionId",rs.getLong("perId"));
					list.add(obj);
				}
				System.out.println("NamedJdBC__> "+list.toString());
				return list;
			}
		});
		System.out.println("out___> "+list);
		
		return new ResponseEntity<String>(list.toString(),HttpStatus.OK);
	}
	
	
	@PostMapping(path = "/path",produces = MediaType.APPLICATION_JSON_VALUE,consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> getString(){
		return null;
	}
}
