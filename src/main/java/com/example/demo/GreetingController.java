package com.example.demo;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/greeting")
public class GreetingController {

	@GetMapping("/hello/{name}")
	public ResponseEntity<String> getHelloGreeting(@PathVariable("name") String name) { 
		System.out.println("Code block for by master..!!");
		System.out.println("Code Added  By Master 1");
		System.out.println("Code ADD By MAIN 1");
		if (!(StringUtils.hasText(name) && Character.isUpperCase(name.charAt(0))))
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		return new ResponseEntity<String>("Hi " + name, HttpStatus.OK);
	}
	@PostMapping("/postGrt")
	public ResponseEntity<String> getPostGrt() {
		return new ResponseEntity<String>("Hi From Post", HttpStatus.OK);
	}
}
